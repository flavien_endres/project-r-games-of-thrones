# Project R - Games Of Thrones

Consulter le projet ici https://lemuriens.shinyapps.io/WikiMaester/

/!\ Si votre écran est trop petit, les éléments peuvent être mal placés! Dézoomer devrait résoudre le problème. ShinyApp ne supporte pas les media querys en CSS, NE PAS CONSULTER AVEC UN TELEPHONE ! /!\


Pour lancer le projet: 

* Télécharger le git.
* Ouvrir RStudio -> File -> Open Project et aller chercher le fichier projet_r.Rproj dans "project_r/main/" .
* Le code peut être consulté en ouvrant app.R .
* Pour lancer le projet, dans la console taper : library(shiny) puis runApp("../main").

